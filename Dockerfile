FROM openjdk:8-jre-alpine
COPY target/my-app-1.5-SNAPSHOT.jar /tmp/my-app-1.5-SNAPSHOT.jar
CMD ["java","-jar","/tmp/my-app-1.5-SNAPSHOT.jar"]